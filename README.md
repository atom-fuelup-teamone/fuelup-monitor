# README #

This is a Mobile first Web app, which can be helpful to track fuel-ups.

### What is this repository for? ###

* Quick summary - Web App to keep track of fuel-ups
* Version - 0.1

### How do I get set up? ###

* Java Web Application Server - used Tomcat
* My SQL Database
* Dependencies mentioned in Maven POM [here](https://bitbucket.org/atom-fuelup-teamone/fuelup-monitor/src/823963befb171bb0e35c4111090ee8dcad498bc8/pom.xml?at=master&fileviewer=file-view-default)
* Database configuration can be modified in Spring Application Context (need to add the Property Placeholder) [here] (https://bitbucket.org/atom-fuelup-teamone/fuelup-monitor/src/823963befb171bb0e35c4111090ee8dcad498bc8/src/main/resources/application-context.xml?at=master&fileviewer=file-view-default)
* How to run tests - not yet setup

### Who do I talk to? ###

* Repo owner or admin - Hiren Parmar (hiren_parmar@outlook.com)